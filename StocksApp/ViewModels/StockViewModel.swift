//
//  StockViewModel.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import Foundation

struct StockViewModel{
    
    let stock: Stock
    
    var symbol: String{
        return self.stock.symbol.uppercased()
    }
    
    var description: String{
        return self.stock.description
    }

    var price:  String{
        return String("$ \(Float(self.stock.price))")
    }
    
    var change: String{
        return self.stock.change
    }
}
