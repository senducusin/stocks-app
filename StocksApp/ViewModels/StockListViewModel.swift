//
//  StockListViewModel.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import Foundation
import SwiftUI

class StockListViewModel: ObservableObject{
    @Published var searchTerm: String = ""
    @Published var stocks:  [StockViewModel] = [StockViewModel]()
    @Published var news:    [NewsArticleViewModel] = [NewsArticleViewModel]()
    @Published var dragOffset:  CGSize = CGSize(width:0, height: 780)
    
    func load(){
        fetchStocks()
        fetchNews()
    }
    
    private func fetchNews(){
        WebService().getTopNews(){ news in
            if let news = news {
                DispatchQueue.main.async {
                    self.news = news.map(NewsArticleViewModel.init)
                }
            }
        }
    }
    
    private func fetchStocks(){
        WebService().getStocks(){ stocks in
            if let stocks = stocks {
                DispatchQueue.main.async {
                    self.stocks = stocks.map(StockViewModel.init)
                }
            }
        }
    }
}
