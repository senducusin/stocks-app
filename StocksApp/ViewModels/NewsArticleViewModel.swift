//
//  NewsArticleViewModel.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/8/21.
//

import Foundation

struct NewsArticleViewModel {
    let article:    Article
    
    var imageUrl:   String{
        return self.article.imageUrl
    }
    
    var title:  String{
        return self.article.title
    }
    
    var author:    String{
        return self.article.author.uppercased()
    }
}
