//
//  ContentView.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var stockListViewModel = StockListViewModel()
    
    init(){
//        Use when in black
//        UINavigationBar.appearance().backgroundColor = .black
//        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UITableView.appearance().backgroundColor = .white
        UITableViewCell.appearance().backgroundColor = .white
        
        stockListViewModel.load()
    }
    
    var body: some View {
        
        let filteredStocks = self.stockListViewModel.searchTerm.isEmpty ? self.stockListViewModel.stocks : self.stockListViewModel.stocks.filter {$0.symbol.starts(with:self.stockListViewModel.searchTerm)}
        
        NavigationView{
            
            ZStack(alignment: .leading) {
                
                Color.white
                
                Text("Stocks")
                    .font(.custom("Arial", size: 32))
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
                        .offset(y: -345)
                
                Text(Date().addingTimeInterval(600), style: .date)
                    .font(.custom("Arial", size: 32))
                    .fontWeight(.bold)
                    .foregroundColor(.gray)
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
                        .offset(y: -305)
                
                SearchView(searchTerm: self.$stockListViewModel.searchTerm)
                    .offset(y:-255)

                StockListView(stocks: filteredStocks)
                    .offset(y:250)
                
                NewsArticleView(newsArticles: self.stockListViewModel.news,
                onDragBegin: {value in
                    
                    self.stockListViewModel.dragOffset = value.translation
                    
                }, onDragEnd: { value in
                    
                    if value.translation.height < 0 {
                        self.stockListViewModel.dragOffset = CGSize(width:0, height:100)
                    }else{
                        self.stockListViewModel.dragOffset = CGSize(width:0, height:780)
                    }
                    
                })
                    .animation(.spring())
                    .offset(y:self.stockListViewModel.dragOffset.height)
            }
            .edgesIgnoringSafeArea(Edge.Set([.bottom, .top]))
            
        }
        .edgesIgnoringSafeArea(Edge.Set([.bottom, .top]))
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
