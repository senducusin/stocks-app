//
//  Article.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/8/21.
//

import Foundation

struct Article:Decodable {
    let title:  String
    let imageUrl:   String
    let author: String 
}
