//
//  Stock.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import Foundation

struct Stock:Decodable {
    let symbol: String
    let description:    String
    let price:  Int
    let change: String
}
