//
//  WebServices.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import Foundation

class WebService{
    
    func getTopNews(completion: @escaping (([Article]?) -> Void)){
        guard let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/top-news") else {
            fatalError("URL is invalid")
        }
        
        URLSession.shared.dataTask(with: url){ data, response, error in
            
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                
                return
            }
            
            let articles = try? JSONDecoder().decode([Article].self, from: data)
            DispatchQueue.main.async {
                completion(articles)
            }
            
        }.resume()
    }
    
    func getStocks(completion:  @escaping (([Stock]?) -> Void)){
        guard let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/stocks") else {
            fatalError("URL is invalid")
        }
        
        URLSession.shared.dataTask(with: url){ data, response, error in
            
            guard let data = data, error == nil else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let stocks = try? JSONDecoder().decode([Stock].self, from: data)
            DispatchQueue.main.async {
                completion(stocks)
            }
            
        }.resume()
    }
}
