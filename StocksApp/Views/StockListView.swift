//
//  StockListView.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/8/21.
//

import SwiftUI

struct StockListView: View {
    
    let stocks: [StockViewModel]
    
    var body: some View {
        List(self.stocks, id: \.symbol){ stock in
            StockCellView(stock: stock)
                .background(Color.white)
                
        }
        .background(Color.white)
    }
}

struct StockListView_Previews: PreviewProvider {
    static var previews: some View {
        let stock = Stock(symbol: "GOOG", description: "Google Stocks", price: 1200, change: "+0.23")
        return StockListView(stocks: [StockViewModel(stock: stock)])
    }
}
