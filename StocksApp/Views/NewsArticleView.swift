//
//  NewsArticleView.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/9/21.
//

import SwiftUI
import URLImage

struct NewsArticleView: View {
    
    let newsArticles:   [NewsArticleViewModel]
    
    let onDragBegin:    (DragGesture.Value) -> Void
    let onDragEnd:  (DragGesture.Value) -> Void
    
    var body: some View {
        
        let screenSize = UIScreen.main.bounds.size
        
        return VStack(alignment: .leading) {
            HStack{
                VStack(alignment:.leading){
                    
                    Text("Top News")
                            .foregroundColor(.white)
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .padding(1)
                   
                    Text("From News")
                        .foregroundColor(.white)
                        .font(.body)
                        .fontWeight(.bold)
                        .padding(2)
                    
                }
                
                Spacer()
            }
            .contentShape(Rectangle())
            .padding()
            .gesture(DragGesture()
                .onChanged(self.onDragBegin)
                .onEnded((self.onDragEnd))
            )
                
                ScrollView{
                    
                    VStack{
                        
                        ForEach(self.newsArticles, id: \.title){ article in
                            HStack{
                                
                                VStack(alignment: .leading){
                                    Text(article.author)
                                        .foregroundColor(.white)
                                        .font( .custom("Arial", size: 22))
                                        .fontWeight(.bold)
                                    
                                    Text(article.title)
                                        .font(.custom("Arial", size: 22))
                                        .foregroundColor(.white)
                                }
                                
                                Spacer()
                                
                                URLImage(url: URL(string:article.imageUrl)!,
                                         content:{
                                            $0.resizable()
                                         })
                                        .frame(width:100, height:100)
                                
                            }
                        }
                        
                    }
                    .frame(maxWidth:.infinity)
                }
            
        }
        .frame(width:screenSize.width, height: screenSize.height)
        .cornerRadius(20)
        .background(Color.gray)
        
    }
}

struct NewsArticleView_Previews: PreviewProvider {
    static var previews: some View {
        
        let article = Article(title: "PayPal is shutting down domestic payments business in India", imageUrl: "https://techcrunch.com/wp-content/uploads/2021/02/GettyImages-1184251295.jpg?w=600", author: "Manish Singh")
        
        return NewsArticleView(newsArticles: [NewsArticleViewModel(article: article)], onDragBegin: {_ in }, onDragEnd: {_ in })
    }
}
