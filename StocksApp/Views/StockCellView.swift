//
//  StockCellView.swift
//  StocksApp
//
//  Created by Jansen Ducusin on 2/8/21.
//

import SwiftUI

struct StockCellView: View {
    
    let stock: StockViewModel
    
    var body: some View {
        return HStack{
            VStack(alignment: .leading) {
                Text(stock.symbol)
                    .font(.custom("Arial", size: 22))
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0))
                
                Text(stock.description)
                    .font(.custom("Arial", size: 18))
                    .foregroundColor(.gray)
            }
            
            Spacer()
            
            VStack{
                Text("\(stock.price)")
                    .foregroundColor(.black)
                    .font(.custom("Arial", size: 22))
                
                Button(stock.change) {}
                    .frame(width:75)
                    .padding(5)
                    .background(Color.red)
                    .cornerRadius(6)
            }
        }
    }
}
