**Stocks App using MVVM**

This is a sample app that utilizes calling of GET HTTP requests, SwiftUI, and Gestures. The source code, demonstrates a simple implementation of MVVM structure in SwiftUI.
  
---

## API

The API used is from a NodeJS with ExpressJS application served in HerokuApp (NodeJS project can be found here: https://github.com/senducusin/jarvis-nodejs.git).  
- Display all stocks: (GET) http://jarvis-nodejs.herokuapp.com/api/stocks  
- Display all top-news: (GET) http://jarvis-nodejs.herokuapp.com/api/top-news

*Note: Server is strictly for testing. Every information from the APIs above are hardcoded and may or may not be accurate data. Data from /top-news are copied from https://newsapi.org/ during the time of making of this app.*

## Features

- Displays the date
- Displays the stocks' information (symbol, name, price, change) in a list format
- The app can search with symbols on the Search bar
- The app has a pull-up news section, which can be accessed by swiping the "Top News" pane upwards